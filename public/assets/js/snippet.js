



$(document).ready(function() {

	  $('.snippet_summary').hide();
	  $('.hide_summary').hide();


	  $(document.body).on("click",".show_summary",function(e){
               	e.preventDefault(e);
               	$('.snippet_summary').hide();
               	$(this).closest('.article_snippet').children('.snippet_summary').show();
              	$(this).next().show();
              	$(this).hide();
             });

 	  $(document.body).on("click",".hide_summary",function(e){
            	e.preventDefault(e);
             	$(this).closest('.article_snippet').children('.snippet_summary').hide();
             	$(this).prev().show();
             	$(this).hide();
         });  

        $(document.body).on("click",".save_link",function(e){
                e.preventDefault(e);
                var id = $(this).next().val(); //article id
                var src= $('#source').val();
                var user_id= $('#user_id').val();
                filter_value= "id=" +id + "&src=" + src + "&user_id=" + user_id;
              $.ajax({
                        url: 'http://dev.news.com/saveresult',
                        type: 'get',
                        data: filter_value,
                        dataType: 'html',
                        success: function (data) { 
                            if(data==0){
                              $.notify("You Must to Logged in to Save.Login Here.","info");
                            }
                            else if(data==1){
                              $.notify("Done!!","success");
                              $(this).prop("disabled", true);
                            }
                            else if(data==2){
                              $.notify("Something is Wrong!!","error");
                            }
                            else if(data==4)
                            {  
                              $.notify("Alerady Saved!!","error");
                              $(this).prop("disabled", true);
                            }
                          }
                });
         });

        $(document.body).on("click",".remove_link",function(e){
                e.preventDefault(e);
                var id = $(this).next().val(); //article id
                //var src= $(this).prev().val();
                var user_id= $('#user_id').val();
                filter_value= "id=" +id + "&user_id=" + user_id;
              $.ajax({
                        url: 'http://dev.news.com/removeresult',
                        type: 'get',
                        data: filter_value,
                        dataType: 'html',
                        success: function (data) { 
                            if(data==0){
                              $.notify("You Must to Logged in to Save.Login Here.","info");
                            }
                            else if(data==1){
                              $.notify("Article Removed!!","success");
                              //$(this).prop("disabled", true);
                              $(this).closest('.article_snippet').hide();
                          }
                            else if(data==2){
                              $.notify("Something is Wrong!!","error");
                            }
                            
                          
                          }
                });
         }); 

}); //document ready close





<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

/*Route::filter('auth', function()
{
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		else
		{
			return Redirect::guest('login');
		}
	}
});*/


/*Route::filter('auth.basic', function()
{
	return Auth::basic();
});*/

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

	/**
	@ redirect to a better page for all filters below 
	*/
Route::filter('guest', function()
{	

   //if the user is logged in and tries to login again redirect him to the home for now	
   if (Auth::check()) 
    {            Session::flash('flash_message', '<b>Opps!!!</b>Seems You are Already Logged in!');
				Session::flash('flash_type', 'alert-error');
                return Redirect::route('home');
    }            
});

Route::filter('auth', function()
{	
        //if the user not logged in and tries to logout or goto dashboard redirect him to the home for now	
        if (Auth::guest()){
      			Session::flash('flash_message', '<b>Opps!!!</b>Seems you are not Logged In.Please Login.');
				Session::flash('flash_type', 'alert-error');
        		return Redirect::route('home');
        }
});

Route::filter('save_auth', function()
{	
        //if the user not logged in and tries to save a article for reading later 
        if (Auth::guest()){
        		return 0;
        }
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});

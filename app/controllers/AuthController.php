<?php

class AuthController extends Controller {


	public function __construct() {
    }
	
	public function facebookLogin()
	{
			require_once(DOCUMENT_PATH."/vendor/hybridauth/hybridauth/hybridauth/Hybrid/Auth.php");
			$config = DOCUMENT_PATH.'/vendor/hybridauth/hybridauth/hybridauth/config.php';
    		$hybridauth = new Hybrid_Auth($config);
   			try{
			    $adapter = $hybridauth->getAdapter("Facebook");
			    $user_profile = $adapter->getUserProfile();
			}
			catch( Exception $e ){
			    // User not connected?
			    if( $e->getCode() == 6 || $e->getCode() == 7 ){ 
			        // log the user out (erase his session locally)
			        $adapter->logout();

			        // try to authenticate again 
			        $adapter = $hybridauth->authenticate("Facebook");
			    }
			}

			$user = User::find($user_profile->identifier);
			if($user){
						Auth::loginUsingId($user_profile->identifier);
						Session::flash('flash_message', 'Logged In Successfully!!');
						Session::flash('flash_type', 'alert-success');
						//return Redirect::intended('dashboard');
						return Redirect::back();
	 				 }
			else{
					if(empty($user_profile->email))
					{
						/**
						@todo redirect to 404 or a better page
						*/
						echo "no email";
						exit;

					}
					$pass=Hash::make($user_profile->identifier);
					$user = User::create(array(
							'id' => $user_profile->identifier,
							'profile_url' => $user_profile->profileURL,
							'profile_image' => $user_profile->photoURL,
							'display_name' => $user_profile->displayName,
							'description' => $user_profile->description,
							'first_name' => $user_profile->firstName,
							'last_name' => $user_profile->lastName,
							'gender' => $user_profile->gender,
							'age' => $user_profile->age,
							'birth_day' => $user_profile->birthDay,
							'birth_month' => $user_profile->birthMonth,
							'birth_year' => $user_profile->birthYear,
							'email' => $user_profile->email,
							'verified_email' => $user_profile->emailVerified,
							'phone' => $user_profile->phone,
							'address' => $user_profile->address,
							'country' => $user_profile->country,
							'region' => $user_profile->region,
							'city' => $user_profile->city,
							'zip' => $user_profile->zip,
							'cover_url' => $user_profile->coverInfoURL,
							'user_name' => $user_profile->username,
							'password' => $pass,
					));
					Auth::loginUsingId($user_profile->identifier);
					Session::flash('flash_message', 'Logged In Successfully!!');
					Session::flash('flash_type', 'alert-success');
					//return Redirect::intended('dashboard');
					return Redirect::back();
		}	
  	}

  	public function dashboard()
  	{	
 		if(Auth::check()){
  			 return View::make('user.dashboard', array('post' =>Read::getAllSavedPost()));	
		}else{
			return Redirect::to("/login");
  		}
	}

	public function logoutFacebook()
	{	
		
		//logout and clear session
		require_once(DOCUMENT_PATH."/vendor/hybridauth/hybridauth/hybridauth/Hybrid/Auth.php");
		$config = DOCUMENT_PATH.'/vendor/hybridauth/hybridauth/hybridauth/config.php';
		$hybridauth = new Hybrid_Auth($config);
		$adapter = $hybridauth->getAdapter("Facebook");
		$adapter->logout();
		Auth::logout();
		return Redirect::back();
	}
}


<?php

class NewsController extends Controller {


	public function __construct() {
       
        
    }

    public function trendingtopics() 
    {
    	$result=renderNews::getNews('topics');
	 	$result = json_decode($result,true);
	 	return View::make('home.constant_news', array('news' => $result,'filter'=>'topics'));		
    }

    public function trends() 
    {
		$result=renderNews::getNews('trends');
		$result = json_decode($result,true);
	 	return View::make('home.constant_news', array('news' => $result,'filter'=>'trends'));		
    }

    public function trendingnews() 
    {
		  $result=renderNews::getNews('news');
		  $result = json_decode($result,true);
		  return View::make('home.constant_news', array('news' => $result,'filter'=>'results'));
    }
}
<?php

class TechblogController extends Controller {


	public function __construct() {
       
    }
	

	public function techcrunch()
	{	
		$result = Techcrunch::where('active', '=', 1)->paginate(8);
		$twitter_feed=renderNews::hashtag('techcrunch')['tweet'];
		$senti=renderNews::hashtag('techcrunch')['sentiment'];
	 	return View::make('tech_blog.tech_article',array('article' => $result,'tweet'=>$twitter_feed,'sentiment'=>$senti,'source'=>'techcrunch'));	
	}

	public function mashable()
	{
		$result = Mashable::where('active', '=', 1)->paginate(8);
		$twitter_feed=renderNews::hashtag('mashable')['tweet'];
		$senti=renderNews::hashtag('techcrunch')['sentiment'];
		return View::make('tech_blog.tech_article',array('article' => $result,'tweet'=>$twitter_feed,'sentiment'=>$senti,'source'=>'mashable'));	
    }

    public function saveresult(){
       	if(Auth::id()==$_GET['user_id']){

    			$check=Read::where('user_id', '=', Auth::id())
        				->where('src', 'like', $_GET['src'])
        				->where('article','=',$_GET['id'])
	        			 ->get();
	        			//exit; 
        		//dd($check);
        		if(!$check->isEmpty())
        		{
        			return 4;
        		}		
    		    else 
    		    {
    		    	  $read = new Read;
                      $read->user_id = Auth::id();
                      $read->src = $_GET['src'];
                      $read->article = $_GET['id']; // this is the id of the article in that corrosponding table(techcrunch or mashable )
                      $read->active = 1;
                      $read->save();
 					  return 1;
 				}	  
    	}
    	else{
    		return 2;
    	}
    }

    public function removeresult(){
        if(Auth::id()==$_GET['user_id']){

                $check=Read::where('user_id', '=', Auth::id())
                        ->where('id','=',$_GET['id']) // this is the id of the article in that corrosponding table(techcrunch or mashable )
                         ->get();
                if($check->isEmpty())
                {
                    return 4; 
                }       
                else 
                {
                      $read = Read::find($_GET['id']); // this is the id in the read table that has to be disabled
                      $read->active = 0;
                      $read->save();
                     // Read::where('id', '=', $_GET['id'])->update(array('active' => 0));
                      return 1;
                }     
        }
        else{
            return 2;
        }
    }
}


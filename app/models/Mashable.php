<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Mashable extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	//The inverse of fillable is guarded, and serves as a "black-list" instead of a "white-list":
	//protected $guarded = array('*');

	protected $fillable =array('id','link','header','summary','active','share');
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'mashable';
	public $timestamps=false;
	
}

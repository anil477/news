<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	@todo protected $fillable = array('first_name', 'last_name', 'email');
	*/
	
	//The inverse of fillable is guarded, and serves as a "black-list" instead of a "white-list":
	//protected $guarded = array('*');

	protected $fillable =array('id','profile_url','photo_image','dispaly_name','description','first_name','last_name','gender','age','birth_day','birth_month','birth_year','email','email_verified','phone','address','country','region','city','zip','cover_url','user_name','password');
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
	public $timestamps=true;
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
	
}

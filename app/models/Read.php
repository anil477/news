<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Read extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	//The inverse of fillable is guarded, and serves as a "black-list" instead of a "white-list":
	//protected $guarded = array('*');

	protected $fillable =array('id','user_id','source','active','article');
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'read';
	public $timestamps=false;
	
	public static function getAllSavedPost(){

		$user_id=Auth::id();
		$allPost=Read::where('user_id', '=', Auth::id())
        			   ->where('active','=',1)
	        			 ->get();
	    $list=array();    			 
	    foreach($allPost as $key=>$value)    			 
	    {
	    	$single_post=DB::select("Select * from {$value->src} where  id={$value->id}");
	    	array_push($list,$single_post);
	    }	
	    return $list;
	}	
}

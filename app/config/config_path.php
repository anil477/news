<?php

/**
 * Configuration
 *
 */

define('DOCUMENT_PATH', '/var/www/larnews/');
define('BASE_HREF', 'http://dev.news.com/');
define('JS_PATH', BASE_HREF.'public/assets/js/');
define('CSS_PATH', BASE_HREF.'public/assets/css/');


 error_reporting(E_ALL);
 ini_set("display_errors", 1);

/**
 * Set the default time-zone
 */
date_default_timezone_set('Asia/Kolkata');


//require_once CONFIG_PATH . 'db_config.php';
?>
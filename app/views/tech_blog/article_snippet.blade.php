
		<div class="article_snippet">
       		<div class="article_heading"> 
       				<h4>	
       					  <a title="Click Here To Read the Original Article" href="{{ $snippet->link }}" target="_blank">{{ $snippet->header }}</a> 
       						<button class="show_summary" title="Show Summary" >>></button> 
       						<button class="hide_summary" title="Hide Summary" ><<</button> 
       						<button class="save_link" title="Save This Link For Viewing Later">^^</button> 
                  <input type="hidden" class="link_id" value="{{ $snippet->id  }}"/>
       				</h4>
			    </div>
       		 <div class="snippet_summary">{{ $snippet->summary }}</div>
   </div> 	

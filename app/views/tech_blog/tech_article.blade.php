@extends('layouts/master')
 


 		   @section('search_bar')
		@parent
		       <form class="navbar-form navbar-left" role="search" action="search" method="get" id="search_bar">
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Search Me" name="term" id="search_query">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
              </form>  		
		 @stop

 @section('container')
 				
 				@if(Auth::check()===true)
 						<input type="hidden" id="user_id" value="{{ Auth::user()->id }}"/>
 				@else
 						<input type="hidden" id="user_id" value="0"/>
 				@endif

 				<input type="hidden" id="source" value="{{ $source }}"/>
				<div id="article">
		 			@foreach ($article as $snippet)
					 	 		@include('tech_blog.article_snippet')
					@endforeach
				</div>
				{{ $article->links() }}
		 @include('home.twitter_feed')
			
 @endsection


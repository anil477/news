 @extends('layouts/master')

 		   @section('search_bar')
		@parent
		       <form class="navbar-form navbar-left" role="search" action="search" method="get" id="search_bar">
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Search Me" name="term" id="search_query">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
              </form>  		
		 @stop
  
     
  
 @section('container')

 				@if(Auth::check()===true)
 						<input type="hidden" id="user_id" value="{{ Auth::user()->id }}"/>
 				@else
 						<input type="hidden" id="user_id" value="0"/>
 				@endif

 					@foreach ($post as $value)
 						@foreach($value as $snippet)
		 						<div class="article_snippet">
							       		<div class="article_heading"> 
							       				<h4>	
							       					  <a title="Click Here To Read the Original Article" href="{{ $snippet->link }}" target="_blank">{{ $snippet->header }}</a> 
							       						
							       						<button class="show_summary" title="Show Summary" >>></button> 
							       						<button class="hide_summary" title="Hide Summary" ><<</button> 
							       						
							       						<button class="remove_link" title="Remove This Link">^^</button> 
							                            <input type="hidden" class="link_id" value="{{ $snippet->id  }}"/>
							       				</h4>
										    </div>
							       		 <div class="snippet_summary">{{ $snippet->summary }}</div>
							   </div>
						@endforeach	    	
					@endforeach


@endsection


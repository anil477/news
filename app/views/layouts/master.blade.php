<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	
  {{ HTML::script( JS_PATH.'jquery.min.js') }}
	{{ HTML::script( JS_PATH.'bootstrap.min.js') }}
  {{ HTML::script( JS_PATH.'snippet.js') }}
  {{ HTML::script( JS_PATH.'notify.min.js') }}

	{{ HTML::style(CSS_PATH.'bootstrap.min.css') }}
	{{ HTML::style(CSS_PATH.'bootstrap-theme.min.css') }}
	{{ HTML::style(CSS_PATH.'custom_header.css') }}
  {{ HTML::style(CSS_PATH.'twitter_feed.css') }}
  {{-- HTML::style(CSS_PATH.'font-awesome.min.css') --}}
  {{ HTML::style(CSS_PATH.'snippet.css') }}
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<title>News Simplified</title>
</head>
<body>
		    <nav class="navbar navbar-default" role="navigation">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="{{ BASE_HREF }}">News Simplified</a>
            </div>

            <div class="collapse navbar-collapse header" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                <li id="results"><a href="{{ BASE_HREF }}trendingnews">Trending News</a></li>
                <li id="trends"><a href="{{ BASE_HREF }}trends">Trending Trends</a></li>
                <li id="topics"><a href="{{ BASE_HREF }}trendingtopics">Trending Topics</a></li>
              </ul>
            
            @yield('search_bar') 
            @show

            <!-- <a href="/login"><img src="public/images/facebook.png" height="30" width="20"></a> -->
              <ul class="nav navbar-nav navbar-right">
               <!--  <li><a href="#">Link</a></li> -->
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Tech Blog<span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="{{ BASE_HREF }}techcrunch">Techcrunch</a></li>
                    <li><a href="{{ BASE_HREF }}mashable">Mashable</a></li>
                  </ul>
                </li>

              @if(Auth::check())
                 <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Hi,{{ Auth::user()->first_name }}<span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="{{ BASE_HREF }}dashboard">Dashboard</a></li>
                    {{-- <li><a href="#">Another action</a></li> --}}
                    <li><a href="{{ BASE_HREF }}logout">Logout</a></li>
                  </ul>
                </li>
              @else
                    <li><a href="{{ BASE_HREF }}login">Login</a></li>
              @endif

              </ul>
            </div>
          </div>
        </nav>
        
      @if(Session::has('flash_message'))
            <div class="alert {{ Session::get('flash_type') }}">
                <h3>{{ Session::get('flash_message') }}</h3>
            </div>
      @endif  

			<div id="container">
					@yield('container')
			</div>
</body> 
</html>

 @extends('layouts/master')

   @section('search_bar')
		@parent
		       <form class="navbar-form navbar-left" role="search" action="search" method="get" id="search_bar">
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Search Me" name="term" id="search_query">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
              </form>  		
		 @stop

	 

 @section('container')

 	<input type="hidden" value="{{ $filter }}" id="select">
 	{{ HTML::script('public/assets/js/header.js') }}
    <?php $news=$news[$filter]; ?>
 	 @if ($filter == 'results')
    		
    		@foreach ($news as $x)
			    <div class="bs-callout bs-callout-success">
			        <h4><a href="{{ $x['url'] }}" title="{{ $x['title'] }}" target="_blank" rel="nofollow"><b>{{ $x['title'] }}</b></a></h4>
			        <p>ucwords({{ $x['kwic'] }})</p>
			        <p><a href="{{ $x['url'] }}" title="{{ $x['title'] }}" target="_blank" rel="nofollow">{{ $x['url'] }}</a></p>
			    </div>
            @endforeach

 	@elseif ($filter == 'topics')
  			
  			@foreach ($news as $x)
			    <div class="bs-callout bs-callout-success">
			        <h4><a href="{{ ($x['related'][0]['url']) }}" title="{{ ($x['related'][0]['title']) }}" target="_blank" rel="nofollow"><b>{{ ($x['related'][0]['title']) }}</b></a></h4>
			        <p>{{ ($x['related'][0]['kwic']) }}</p>
			        <p><a href="{{ ($x['related'][0]['url']) }}" title="{{ ($x['related'][0]['title']) }}" target="_blank" rel="nofollow">{{ ($x['related'][0]['url']) }}</a></p>
			    </div>
            @endforeach

	@elseif ($filter == 'trends')

    		@foreach ($news as $x)
			    <div class="bs-callout bs-callout-success">
			        <h4><a href="search/<?php echo str_replace(" ", "-", $x) ?>" rel="nofollow" title="{{ $x }}"><b>{{ ($x) }}</b></a></h4>
			    </div>
            @endforeach
	@else
			Sorry NO Result Found.
	@endif
@endsection

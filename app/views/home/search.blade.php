 @extends('layouts/master')

  @section('search_bar')
	
		@parent
		       <form class="navbar-form navbar-left" role="search" action="search" method="get" id="search_bar">
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Search Me" name="term" value="{{ $query_to_set }}" >
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
              </form>  		
		 @stop

 @section('container')

 	{{ HTML::script('public/assets/js/header.js') }}
    {{--	@foreach ($news as $x)
			    <div class="bs-callout bs-callout-success">
			        <h4><b>{{ $x['title'] }}</b></h4>
			        <p>{{ $x['kwic'] }}</p>
			        <p><a href="{{ $x['url'] }}" title="{{ $x['title'] }}" target="_blank">{{ $x['url'] }}</a></p>
			    </div>
        @endforeach   
		
        @foreach ($web as $x)
			    <div class="bs-callout bs-callout-info">
			        <h4><b>{{ $x['title'] }}</b></h4>
			        <p>{{ $x['kwic'] }}</p>
			        <p><a href="{{ $x['url'] }}" title="{{ $x['title'] }}" target="_blank">{{ $x['url'] }}</a></p>
			    </div>
        @endforeach
	--}}
	<?php
		foreach($web as $result){
            
             echo " <div class='bs-callout bs-callout-success'>";
             echo "<h4><b>".utf8_decode($result->getTitle())."</b></h4>";
             echo "<p><a href='".$result->getUrl()."' target='_blank'>".$result->getUrl()."</a></p>";
             echo "</div>";
         }
     ?>

     <?php
		foreach($news as $result){
            
             echo " <div class='bs-callout bs-callout-info'>";
             echo "<h4><b>".utf8_decode($result->getTitle())."</b></h4>";
             echo "<p><a href='".$result->getUrl()." 'target='_blank'>".$result->getUrl()."</a></p>";
             echo "</div>";
         }
     ?>	
@endsection




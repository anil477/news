<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


/*Route::get('/', array('as' => 'home',function()
	{ 
		Read::getAllSavedPost();exit;
		return View::make('home.index'); 

	} 
));
*/

Route::get('/', array('as' => 'home',function(){ return View::make('home.index'); } ));
Route::get('/dashboard', array('as' => 'dashboard','uses' => 'AuthController@dashboard'))->before('auth'); //login redirects here-basically for better url
Route::get('/login', array('as' => 'login','uses' => 'AuthController@facebookLogin'))->before('guest');
Route::get('/logout', 'AuthController@logoutFacebook')->before('auth');//logout 
Route::get('/techcrunch', 'TechblogController@techcrunch');//to render techcrunch article
Route::get('/mashable', 'TechblogController@mashable');//to render mashable article  
Route::get('/trendingnews', 'NewsController@trendingnews');  //to render trending news
Route::get('/trends', 'NewsController@trends'); //to render trending trends
Route::get('/trendingtopics', 'NewsController@trendingtopics'); //to render trending topics



Route::get('/saveresult', 'TechblogController@saveresult')->before('save_auth'); //save links 
Route::get('/removeresult', 'TechblogController@removeresult')->before('save_auth'); //save links 

Route::get('/search/{query?}', function($query=null)
{
    $name = Input::get('term');
	if($query !=null)
			{   
				$query_to_set=str_replace("-", " ", $query);
				$query= rawurlencode($query);
				//$query= rawurlencode( preg_replace("([/=():;])",'',$query)); //remove char that are not allowed  in api

			}	
	//if query form submitted redirect it back with a clean format for a clean url
	if($name!='')
		{ 
			$name=str_replace(" ", "-", $name);
			return Redirect::to("/search/{$name}");
			// because the url will be messy so we will redirect it back to get a cleaner url
			// the messy url -  http://localhost/larnews/search?term=anil%20kumble
			// the clean url -  http://localhost/larnews/search/anil%20kumble

		}
	else if($query !=null) // when redirected it will go through this 
		{ 
			$result=renderNews::searchResult($query); 
		}
	else //handle a blank query 	
		{	
			return Redirect::to("/trendingnews");
		}

	$web=$result[0];
	$news=$result[1];
    return View::make('home.search', array('web' => $web,'news' => $news,'filter'=>'results','query_to_set'=>$query_to_set));		
});


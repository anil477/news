<?php
 use Sunra\PhpSimple\HtmlDomParser;
/**
 * @author anil
 */
class renderNews{

	//render trending topic,trending trends and trending news
    public static function getNews($param) {
        
        $url="http://www.faroo.com/api?q=&start=1&length=10&l=en&src=".$param."&f=json&key=Tf9@y14AptkXcnmoTCxyt@UDFCs_";         
        $ch = curl_init($url);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
            )
        );
        return curl_exec($ch);
      }


     //get twitter hastag for any query 
    public static function hashtag($query) {
                $query=rawurlencode($query); //handle multiple query api 
                $result=array(); //this returns the tweet as well as the sentiment analysis
                
                /** 
                @todo replace these id in the config file 
                */
                
                /** Set access tokens here - see: https://dev.twitter.com/apps/ **/
                $settings = array(
                    'oauth_access_token' => "183298099-MQlOsFmfTIr2fqYPkKAGdxOhA9iwPSZ1TYJggcgl",
                    'oauth_access_token_secret' => "kpjfg9hBi2MtjFwHbI6ohT1Nsd44cu42dleGwm4Us5uhz",
                    'consumer_key' => "bhDtyN6EAApWJ4RTZQVwk6MCw",
                    'consumer_secret' => "4RUyNsPrwy3a3zqtGzRE6ajcoIPaF2b1FVR0fiGLmSrUKQNVpi"
                );

                //reference--https://dev.twitter.com/rest/reference/get/search/tweets
                $url = 'https://api.twitter.com/1.1/search/tweets.json';
                $requestMethod = 'GET';
                $getfield = "?q=#{$query}&result_type=recent&count=10";

                // Perform the request
                $twitter = new TwitterAPIExchange($settings);
                $twitter_feed= $twitter->setGetfield($getfield)
                             ->buildOauth($url, $requestMethod)
                             ->performRequest();
                 //return an assocotivae array from json data received
                 $twitter_feed=json_decode($twitter_feed,true)['statuses'];
                 //return $twitter_feed;
                    $feed=array();//tweet  feed to return- editing json bcoz we neeed just a few details
                    $counter=0;                 
                 $post="";   
                /** 
                    @todo handling multiple links in post 
                */
                 foreach ($twitter_feed as $key => $value) {
                     $feed[$counter]['post']=$value['text'];
                     $post=$post."  ".$value['text']."<br>";
                     $url=preg_match('/(http)(.*?)( )/', $value['text']." ", $m);
                     $feed[$counter]['url']=reset($m);
                     $feed[$counter]['time']=substr($value['created_at'],0,-11);
                     $counter+=1;   
                 }
                 $result['sentiment']=$sentiemnt=renderNews::getSentiment($post);
                 $result['tweet']=$feed;
                 return $result;
      }

     //refer- http://sentiment.vivekn.com/docs/api/
     //returns sentiemnt for the post  
     public static function getSentiment($post)
     {
            $ch = curl_init("http://sentiment.vivekn.com/api/text/");
            $postData=http_build_query(array(
                             "txt" => $post
                        ));
            curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_POST, count($postData));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);    
            return json_decode(curl_exec ($ch),true);
     } 


     //returns the search result for any parametrer passed	
     public static function searchResult($param) {
        
        $param= rawurldecode($param);
        $param=str_replace("-", " ", $param);
        $googleUrl=new \GoogleUrl();
        $googleUrl->setLang('en') // lang allows to adapt the query (tld, and google local params)
            ->setNumberResults(10);         
        $result_web=$googleUrl->setPage(0)->search("{$param}"); // result_web results page 1 (results 1-20)
        $positions1=$result_web->getPositions();

        $param=$param.' news';
        $result_news=$googleUrl->setPage(0)->search("{$param}"); // result_news results page 1 (results 1-20)
        $positions2=$result_news->getPositions();

        $result=array();
        array_push($result,$positions1);
        array_push($result,$positions2);
        return $result;  
    }

		//function to summarize any article with a url	   
        public static function summarize_artcile($url) {
        
                $ch = curl_init("http://www.tools4noobs.com/");
                $postData=http_build_query(array(
                     "action" => "ajax_summarize",
                "url" => $url,
                "text" => '',
                "threshold" => 70,
                "treshold_lines"=>'',
                "min_sentence_length"=>50,
                "min_word_length"=>4,
                "first_best"=>10
                ));
                curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch, CURLOPT_POST, count($postData));
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);    
                $str=curl_exec ($ch);
                curl_close($ch);
                $patt="/(<li>)(.*?)(<\/li>)/";
                preg_match_all($patt, $str, $matches);
                   
                $arcticle="";
                foreach ($matches[2] as $key => $value) {
                     $arcticle.=$value;
                } 
                return $arcticle;
        }

            //techcrunch all article
            public static function techcrunch()
            {	
              $html = HtmlDomParser::file_get_html("http://techcrunch.com/");
            	$all=array();
            	    foreach($html->find('h2.post-title') as $e)
          				{		 
          					foreach($e->find('a') as $x)
            					{  
            						$temp['header']=$x->innertext;
            						$temp['link']= $x->href;
            						$temp['summary']=self::summarize_artcile($x->href);
                        array_push($all,$temp);
            					}	
          			  }
              $html = HtmlDomParser::file_get_html("http://techcrunch.com/popular/");
            	$all_popular=array();
            	foreach($html->find('h2.post-title') as $e)
      				{		 
      					foreach($e->find('a') as $x)
        					{  
        			          $temp['header']=$x->innertext;
                        $temp['link']= $x->href;
                        $temp['summary']=self::summarize_artcile($x->href);
                        array_push($all_popular,$temp);
                  }	
      			  }
				        $techcrunch=array_merge($all,$all_popular);
                $techcrunch=array_map("unserialize", array_unique(array_map("serialize", $techcrunch)));
                /** 
                @todo this database writing is to done by running cron
                */
                foreach($techcrunch as $key=>$value)
                { 
                  /*DB::insert('Insert into techcrunchs (link,header,summary,active,share) values (?,?,?,?,?)', 
                              array($value['link'], $value['header'],$value['summary'],'1','0'));*/
                      $techcrunch = new Techcrunch;
                      $techcrunch->link = $value['link'];
                      $techcrunch->header = $value['header'];
                      $techcrunch->summary = $value['summary'];
                      $techcrunch->active = 1;
                      $techcrunch->share = 0;
                      $techcrunch->save();
                }
                //return $techcrunch;                
             }

            //mashable all article
            public static function mashable()
            {	
            	$html = HtmlDomParser::file_get_html("http://mashable.com/");
            	$all=array();
                  foreach($html->find('h1') as $e)
          				{		 
          					foreach($e->find('a') as $x)
            					{  
                                $temp['header']=$x->innertext;
                                $temp['link']= $x->href;
                                $temp['summary']=self::summarize_artcile($x->href);
                                array_push($all,$temp);
            					}	
          			  }
              $html = HtmlDomParser::file_get_html("http://mashable.com/tech/");
            	$all_tech=array();
            	foreach($html->find('h1') as $e)
  				    {		 
        					foreach($e->find('a') as $x)
          					{  
          						        $temp['header']=$x->innertext;
                              $temp['link']= $x->href;
                              $temp['summary']=self::summarize_artcile($x->href);
                              array_push($all_tech,$temp);
          					}	
        			}
                $mashable=array_merge($all,$all_tech);
                $mashable=array_map("unserialize", array_unique(array_map("serialize", $mashable)));
                foreach($mashable as $key=>$value)
                { 
                      
                      $mashable = new Mashable;
                      $mashable->link = $value['link'];
                      $mashable->header = $value['header'];
                      $mashable->summary = $value['summary'];
                      $mashable->active = 1;
                      $mashable->share = 0;
                      $mashable->save();
                }
                //return $mashable;                
             }

}//class ends here 

